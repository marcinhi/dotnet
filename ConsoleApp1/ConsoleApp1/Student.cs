﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dao
{
    class Student
    {
        public int StudentID { get; set; }
        public string StudentName { get; set; }
        public int Age { get; set; }

        public Student(int id, string name, int age){
            this.StudentID = id;
            this.StudentName = name;
            this.Age = age;
        }

        public Func<Student, bool> isTeenager = s => s.Age > 12 && s.Age < 20;
        public bool IsTeenagerMethod(Student student)
        {
            return student.Age > 12 ? true : false;
        }
    }
}
