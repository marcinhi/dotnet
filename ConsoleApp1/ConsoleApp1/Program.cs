﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dao;
namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Student student = new Student(1, "Adam", 50);
            Console.WriteLine(student.StudentName);
            bool snd = student.IsTeenagerMethod(student);
            Console.WriteLine(snd);
            List<Student> listOfStudents = new List<Student>();
            listOfStudents.Add(student);
            
            IEnumerable<bool> enumerable = listOfStudents.Select(student.isTeenager);
            Console.WriteLine(enumerable); //does not display value
        }
    }
}
