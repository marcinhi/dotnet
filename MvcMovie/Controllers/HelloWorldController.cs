﻿using Microsoft.AspNetCore.Mvc;

namespace MvcMovie.Controllers
{
    public class HelloWorldController : Controller
    {
        // 
        // GET: /HelloWorld/

        public IActionResult Index()
        {
            //return "This is my default action...";
            return View();

        }

        // 
        // GET: /HelloWorld/Welcome/ 

        public IActionResult Welcome(string name, string surname, int id = 5)
        {
            //return "This is the Welcome action method...";
            //return HtmlEncoder.Default.Encode($"Hello {name}, Surname {surname}, Age is:{age}");// we use it only for learning, not used normally
            int age = id;
            ViewData["Message"] = "Hello " + name + " " + surname;
            ViewData["Age"] = age;

            return View();
        }
    }
}